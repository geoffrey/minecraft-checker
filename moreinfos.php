﻿<?php
date_default_timezone_set('Europe/Paris');
if (isset($_GET['i']))
{
	$data = new DomDocument();
	$data->load('data.xml');
	$dataNode = $data->getElementsByTagName('data')->item(0);
	$mcupdatesNode = $dataNode->getElementsByTagName('mcupdates')->item(0);
	$releaseS = $mcupdatesNode->getElementsByTagName('release');
	// $lastVersions_e = explode('|', file_get_contents('lastVersions.txt'));
	$prevRelease = false;
	for ($c = $releaseS->length, $i = 0; $i < $c; $i++) {
		$release = $releaseS->item($i);
		if ($_GET['i'] == $release->getAttribute('id')) {
			$releaseInfos = $release->getElementsByTagName('infos')->item(0);
			$releaseDateE = explode('/', $releaseInfos->getAttribute('date'));
			$date = $releaseInfos->getAttribute('date');
			echo 'Sortie le '.date('d', $date).'/'.date('m', $date).'/'.date('Y', $date);
			/*if (date('G', $date) != 0 OR date('i', $date) != 0) { 
				echo ' à '.date('G', $date).':'.date('i', $date).' (environ)'; 
			}*/
			echo '.<br/>';
			if ($release->getAttribute('name') != $releaseInfos->getAttribute('givenName')) {
				echo 'Le nom de la version donné dans le jeu est '.$releaseInfos->getAttribute('givenName').' mais le nom réel est '.$release->getAttribute('name').'.<br/>';
			}
			// Téléchargement
			echo '<div class="download"><h4>Téléchargement</h4>
Nom : <strong>minecraft.jar</strong>
Taille : <strong>'.$releaseInfos->getAttribute('size').' octets</strong><br/>
MD5 : <strong>'.$releaseInfos->getAttribute('md5').'</strong><br/>
SHA-1 : <strong>'.$releaseInfos->getAttribute('sha1').'</strong><br/>';
			if (is_dir($releaseInfos->getAttribute('directory'))) {
				$downloadLocal = $releaseInfos->getAttribute('directory').'/minecraft.jar'; }
			if (@fopen($releaseInfos->getAttribute('link'), 'r') AND ($release->getAttribute('type') != 'r' OR $i+1 == $c)) {
				$downloadServer = $releaseInfos->getAttribute('link');
			} else {
				// TODO AR? Prévoir ça (variable $changes par exemple)
			}
			// TODO AR $downloadWiki
			if (isset($downloadLocal) OR isset($downloadServer) OR isset($downloadWiki)) {
				if (isset($downloadLocal)) {
				echo '<a href="'.$downloadLocal.'">Depuis ce serveur</a><br/>'; }
				if (isset($downloadServer)) {
				echo '<a href="'.$downloadServer.'">Depuis minecraft.net</a><br/> '; }
				if (isset($downloadWiki)) {
				echo '<a href="'.$downloadWiki.'">Depuis Minecaft Wiki</a><br/> '; }	
			} else {
				echo 'Aucun lien de téléchargement n\'est disponible.';
			}
			echo '</div>';
			echo '<div class="changelog">
<h4>Changelog</h4>
Langue : Français<br/>
<a class="wikiLink" href="'.$releaseInfos->getAttribute('wiki').'">Lien vers le wiki</a><br/>
</div>';
			$text = '<ul>';
			if ($release->getElementsByTagName('files')->item(0)) {
				echo '<div class="files">
<h4>Fichiers du .jar</h4>';
				$directory = $releaseInfos->getAttribute('directory');
				$haveLink = (is_dir($directory) AND is_dir($directory.'/content'));
				$elementS = $release->getElementsByTagName('files')->item(0)->childNodes;
				$filesModificationsStat = array(0, 0, 0, 0);
				$directoryXML = directoryXML($elementS, $releaseInfos->getAttribute('directory').'/content');
				if ($prevRelease)
					echo 'Comparaison par rapport à la version : '.$prevRelease->getAttribute('name').'.<br/>';
				echo '<span class="changeStats">'.$filesModificationsStat[3].' fichiers au total, '.$filesModificationsStat[2].' inchangés, '.$filesModificationsStat[1].' ajoutés et '.$filesModificationsStat[0].' modifiés (dont fichiers obstrués). <input class="releaseFilesButton" type="button" value="+" /></span><br/>';
				// TODO Supprimer (s) (conditions)
				echo '<div class="releaseFiles" style="display: none;"><p>Tri selon les fichiers modifiés : <input type="radio" name="filesSelectionChangement" class="fileSelectionChangementAll" title="Pas de filtre pour les modifications de fichier" checked /><input type="radio" name="filesSelectionChangement" class="fileSelectionChangementChanged" title="Filtrer les fichiers restés inchangés" /><input type="radio" name="filesSelectionChangement" class="fileSelectionChangementAdded" title="Filtrer les fichiers uniquement modifiés (ne garder que les fichiers ajoutés)"/></p><p>
Tri selon le type de fichier : <input type="radio" name="filesSelectionClass" class="fileSelectionClassAll" title="Pas de filtre pour les types de fichiers"/><input type="radio" name="filesSelectionClass" checked class="fileSelectionClassNontechnic" title="Filtrer les fichiers obstrués"/><input type="radio" name="filesSelectionClass" class="fileSelectionClassNone" title="Filtrer les fichiers .class"/></p><div class="releaseFilesContainer">
<strong class="minecraftDotJar">Minecraft.jar</strong><br/><ul>'.$directoryXML.'</ul></div></div>';
				echo '</div>';
			}
			exit();
		}
		else {
			$prevRelease = $release;
		}
	}
	echo 'ERREUR L\'information demmandée n\'a pas été trouvée.<br/>
Si il s\'agit d\'une sortie récente, attendez un peu et réésayez.';
} else {
	echo 'ERREUR Les informations demmandées ne sont pas correctes.';
}
function directoryXML($XML, $path) {
	global $filesModificationsStat;
	global $haveLink;
	$text = '';
	for ($c = $XML->length, $i = 0; $i < $c; $i++) {
		$element = $XML->item($i);
		if ($element->tagName == 'dir') {
			if ($directoryXML = directoryXML($element->childNodes, $path.'/'.$element->getAttribute('name'))) {
				$text .= '<li><span class="dir closed">'.$element->getAttribute('name').'</span><ul class="dirUl" style="display: none;">'.$directoryXML.'</ul></li>';
			}
		} elseif ($element->tagName == 'file') {
		/*
		if ((isset($_GET['allowClass']) OR strlen(@$exploded[0]) > 3 OR @$exploded[1] != 'class' OR $element->getAttribute('comparaison')== 'addded') AND (isset($_GET['allowSame']) OR $element->getAttribute('comparaison')!= 'same')) { */
				$className = 'file';
				switch ($element->getAttribute('comparaison')) {
					case 'change':
					$comparaison = '≠';
					$className .= ' change';
					$filesModificationsStat[0]++;
					break;
					case 'added':
					$comparaison = '+';
					$className .= ' added';
					$filesModificationsStat[1]++;
					break;
					case 'same';
					$comparaison = '=';
					$className .= ' same';
					$filesModificationsStat[2]++;
					break;
					default:
					$comparaison = '?';
					break;
				}
				$exploded = explode('.', $element->getAttribute('name'));
				if (@$exploded[1] == 'class') {
					$className .= ' class';
				}
				if (strlen($exploded[0]) < 4 AND @$exploded[1] == 'class') {
					$className .= ' technic';
				}
				if (@$exploded[1] == 'png') {
					$className .= ' image';
				}
				if (@$exploded[1] == 'txt') {
					$className .= ' txt';
				}
				$filesModificationsStat[3]++;
				$text .= '<li><span class="'.$className.'">';
				if ($haveLink) { $text.= '<a href="'.$path.'/'.$element->getAttribute('name').'">'; }
				$text .= $element->getAttribute('name');
				if ($haveLink) { $text .= '</a>'; }
				$text .= '</span>('.$comparaison.')</li>';  
		}
	}
return $text;
}

?>