<?php
$data = new DomDocument();
$data->load('data.xml');
$dataNode = $data->getElementsByTagName('data')->item(0);
$mcupdatesNode = $dataNode->getElementsByTagName('mcupdates')->item(0);
$releaseS = $mcupdatesNode->getElementsByTagName('release');
$text = '';
for ($i = $releaseS->length; $i > 1; $i--) {
	$release = $releaseS->item($i-1);
	$releaseInfos = $release->getElementsByTagName('infos')->item(0);
	$text .= '<h3>Minecraft '.$release->getAttribute('name').' ';
	if (isset($prevRelease)) {
		if ($release->getAttribute('name') == $prevRelease->getAttribute('name')) {
			$text .= '(version enterrée) ';
		}
	}
	$shortDescription = $releaseInfos->getAttribute('shortDescription');
	if ($shortDescription == 'false') {
		$shortDescription = 'Pas de description.';
	}
	$text .= '</h3><div class="release" id="R'.$release->getAttribute('id').'"><div class="shortDescription">'.$shortDescription.' <input type="button" value="+" class="more" /></div><div class="content"></div></div>';
	$prevRelease = $release;
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
<title>Minecraft Checker</title>
<meta charset="utf-8">
<link rel="stylesheet" media="screen" type="text/css" title="design" href="design.css">
<script type="text/javascript">
var checkIntervalForm;
var checkState;
var checkTimeout;
var addXhr;
var allReleases;
var checkXhr;
var lastId = <?php echo $mcupdatesNode->getAttribute('total'); ?>;

window.onload = function () {
checkIntervalForm = document.getElementById('checkActivate');
checkState = document.getElementById('checkState');
checkIntervalForm.addEventListener('change', function(e) {
	if (e.target.checked) {
		checkState.innerHTML = 'activé';
		checkTimeout = true;
		check();
		console.log('Checking activated');
	} else {
		checkState.innerHTML = 'désactivé';
		clearTimeout(checkTimeout); // TODO Annulation de checkXhr
		checkTimeout = false;
		checkXhr.abort();
		console.log('Checking stopped');
	}
}, false);

allReleases = document.getElementById('allreleases');
allReleaseS = allReleases.getElementsByClassName('more');
for (var allReleaseI = 0; allReleaseI < allReleaseS.length; allReleaseI++) {
	allReleaseS[allReleaseI].addEventListener('click', releaseOpen, false);
}
console.log('System initialised');
}
function releaseOpen(e) {
e.target.value = '...';
var releaseXhr = new XMLHttpRequest();
releaseXhr.onreadystatechange = function() {
	if (releaseXhr.readyState == 4 && (releaseXhr.status == 200 || releaseXhr.status == 0)) {
		e.target.removeEventListener('click', releaseOpen, false);
		console.log('	Infos recevied');
		var content = e.target.parentNode.parentNode.getElementsByClassName('content')[0];
		content.innerHTML = releaseXhr.responseText;
		if (content.getElementsByClassName('releaseFiles')[0]) {
		content.getElementsByClassName('releaseFilesButton')[0].addEventListener('click', releaseFilesOpen, false);
		filesDirExpandS = content.getElementsByClassName('dir');
			for (var filesDirExpandI = 0; filesDirExpandI < filesDirExpandS.length; filesDirExpandI++) {
				filesDirExpandS[filesDirExpandI].addEventListener('click', releaseFilesDirOpen, false);
			}
			var releaseFilesSelectionS = content.getElementsByClassName('releaseFiles')[0].getElementsByTagName('input');
			for (var releaseFilesSelectionI = 0; releaseFilesSelectionI < releaseFilesSelectionS.length; releaseFilesSelectionI++) {
				if (releaseFilesSelectionS[releaseFilesSelectionI].name == 'filesSelectionChangement' || releaseFilesSelectionS[releaseFilesSelectionI].name == 'filesSelectionClass') {
					releaseFilesSelectionS[releaseFilesSelectionI].addEventListener('click', releaseFilesOrderEvent, false);
				}
			}
		}
		e.target.value = '-';
		e.target.addEventListener('click', releaseClose, false);
	}
};
var releaseId = e.target.parentNode.parentNode.id.split('R')[1];
console.log('Searching details for release ID = '+releaseId+'.');
releaseXhr.open('GET', 'moreinfos.php?i='+releaseId, true);
releaseXhr.send(null);
}
function releaseFilesDirOpen(e) {
	e.target.parentNode.getElementsByClassName('dirUl')[0].style.display = '';
	e.target.className = 'dir opened';
	e.target.removeEventListener('click', releaseFilesDirOpen, false);
	e.target.addEventListener('click', releaseFilesDirClose, false);
}
function releaseFilesDirClose(e) {
	e.target.parentNode.getElementsByClassName('dirUl')[0].style.display = 'none';
	e.target.className = 'dir closed';
	e.target.removeEventListener('click', releaseFilesDirClose, false);
	e.target.addEventListener('click', releaseFilesDirOpen, false);
}
function releaseClose(e) {
	e.target.value = '...';
	e.target.parentNode.parentNode.getElementsByClassName('content')[0].innerHTML = '';
	e.target.removeEventListener('click', releaseClose, false);
	e.target.addEventListener('click', releaseOpen, false);
	e.target.value = '+';
}
function releaseFilesOpen(e) {
	e.target.value = '...';
	var releaseFiles = e.target.parentNode.parentNode.getElementsByClassName('releaseFiles')[0];
	releaseFiles.style.display = '';
	releaseFilesOrder(releaseFiles);
	e.target.removeEventListener('click', releaseFilesOpen, false);
	e.target.addEventListener('click', releaseFilesClose, false);
	e.target.value = '-';
}
function releaseFilesOrder(releaseFiles) {
	var releaseFilesFileS = releaseFiles.getElementsByClassName('file');
	var releaseFilesSelectionClassAll = releaseFiles.getElementsByClassName('fileSelectionClassAll')[0].checked;
	var releaseFilesSelectionClassNonTechnic = releaseFiles.getElementsByClassName('fileSelectionClassNontechnic')[0].checked;
	var releaseFilesSelectionClassNone = releaseFiles.getElementsByClassName('fileSelectionClassNone')[0].checked;
	var releaseFilesSelectionChangementAll = releaseFiles.getElementsByClassName('fileSelectionChangementAll')[0].checked;
	var releaseFilesSelectionChangementChanged = releaseFiles.getElementsByClassName('fileSelectionChangementChanged')[0].checked;
	var releaseFilesSelectionChangementAdded = releaseFiles.getElementsByClassName('fileSelectionChangementAdded')[0].checked;
	for (var releaseFilesFileI = 0; releaseFilesFileI < releaseFilesFileS.length; releaseFilesFileI++) {
		var releaseFilesFileClassE = releaseFilesFileS[releaseFilesFileI].className.split(' ');
		var releaseFilesFileTechnic = in_array('technic', releaseFilesFileClassE);
		var releaseFilesFileClass = in_array('class', releaseFilesFileClassE);
		var releaseFilesFileChanged = in_array('change', releaseFilesFileClassE);
		var releaseFilesFileAdded = in_array('added', releaseFilesFileClassE);
		if ((releaseFilesSelectionClassAll || (releaseFilesSelectionClassNonTechnic && (!releaseFilesFileTechnic)) || (releaseFilesSelectionClassNone && (!releaseFilesFileClass))) && (releaseFilesSelectionChangementAll || (releaseFilesSelectionChangementChanged && (releaseFilesFileChanged || releaseFilesFileAdded)) || (releaseFilesSelectionChangementAdded && releaseFilesFileAdded))) {
			releaseFilesFileS[releaseFilesFileI].parentNode.style.display = '';
		} else  {
			releaseFilesFileS[releaseFilesFileI].parentNode.style.display = 'none';
		}
	}
}
function releaseFilesOrderEvent(e) {
	releaseFilesOrder(e.target.parentNode.parentNode);
}
function releaseFilesClose(e) {
	e.target.value = '+';
	e.target.parentNode.parentNode.getElementsByClassName('releaseFiles')[0].style.display = 'none';
	e.target.removeEventListener('click', releaseFilesClose, false);
	e.target.addEventListener('click', releaseFilesOpen, false);
}
function check() {
checkXhr = new XMLHttpRequest();
checkXhr.onreadystatechange = function() {
	if (checkXhr.readyState == 4 && (checkXhr.status == 200 || checkXhr.status == 0)) {
		console.log('Check finished, with information : '+checkXhr.responseText);
		var newReleaseE = checkXhr.responseText.split('|');
		if (newReleaseE.length == 8 && checkTimeout) {
			if (newReleaseE[5] == lastId) {
				console.log('	Information analysed : no new release (date: '+newReleaseE[0]+').');
				checkState.innerHTML = 'attente';
			} else if (newReleaseE[5] > lastId) {
				console.log('	Information analysed : new release.');
				checkState.innerHTML = 'nouvelle version';
				// TODO Vérifier si (newReleaseE[5] == lastId+1)
				lastRelease = newReleaseE[6];
				if (newReleaseE[7] == 'r' && newReleaseE[5] == lastId+1) {
					var newReleaseAlertType = 'La release complète';
				} else if (newReleaseE[7] == 'apr' && newReleaseE[5] == lastId+1) {
					var newReleaseAlertType = 'La pre-release automatique';
				} else if (newReleaseE[7] == 'pr' && newReleaseE[5] == lastId+1) {
					var newReleaseAlertType = 'La pre-release';				
				} else {
					console.error('	Bad informations on the new release. Reloading page.');
					checkState.innerHTML = 'erreur';
					location.reload();
					return false;
				}
				lastId = newReleaseE[5];
				console.info('New release: name: '+lastRelease+' id:'+newReleaseE[5]+' type: '+newReleaseE[7]+'.');
				checkState.innerHTML = 'nouvelle version';
				document.title='Version '+lastRelease+' sortie !';
				alert(newReleaseAlertType+' '+lastRelease+' est sortie !');
				location.reload();
			} else {
				console.error('Database upset. Reloading page.');
				checkState.innerHTML = 'erreur';
				location.reload();
			}
			checkTimeout = setTimeout('check()', 5000);
		} else if (!checkTimeout) {
			return false;
		} else {
			console.error('Error comited server-side.');
			checkState.innerHTML = 'erreur';
		}
	}
};
checkXhr.open('GET', 'test.php', true);
checkState.innerHTML = 'recherche';
checkXhr.send(null);
}

function in_array(needle, haystack) {
	for (key in haystack) {
		if (haystack[key] == needle) {
			return true;
		}
	}
	return false;
}
</script>
</head>
<body>
<h1>Minecraft Checker</h1>
<div id="body">
	<h2>Rechercher des versions</h2>
	<p class="RL_search"><input type="checkbox" id="checkActivate" name="checkActivate"/><label for="checkActivate"> Rechercher les nouvelles releases (<span id="checkState">désactivé</span>)</label></p>

	<h2>Versions référencées</h2>
	<div id="allreleases">
		<?php echo $text; ?>
	</div>
</div>
<footer>
<p id="copyright">Copyright &copy; <a href="https://twitter.com/#!/WebFrogeye">WebFrogeye</a> 2012</p>
<p>Créé par <a href="https://twitter.com/#!/WebFrogeye">@WebFrogeye</a>.<br/>
Avec l'aide d'<a href="https://twitter.com/#!/EphysPotato">@EphysPotato</a>.<br/>
Sur un design de <a href="http://dlp.host56.com/">DLProduction</a>.<br/>
D'après un script de <a href="https://twitter.com/#!/shellgratuit">@shellgratuit</a>.</p>
</footer>
<div id="cache" style="display: none;"></div>
</body>
</html>